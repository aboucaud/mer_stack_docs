MER stack and pipeline development
==================================

The following page gathers general information on code development for Euclid with some OU-MER specificities.

## Overview

To make this page as useful as possible, it starts with information about versioning in order to allow for code sharing first. Then it presents the Euclid development framework (rules + tools) with appropriate links and finally describes the way to write or modify a code to be fully Euclid compliant.

- [Code sharing and versioning](#code-sharingversioning)
    - [Initial upload of code](#initial-upload-of-code-on-the-repository)
    - [Working on existing code](#working-on-an-existing-project)
- [Euclid development tools and rules](#euclid-development)
    - [EDEN](#eden)
    - [LODEEN](#lodeen)
    - [Elements](#elements)
    - [Quick start tutorial](#tutorial)
- [Euclid compliant code](#euclid-compliant-code)
    - [CODEEN](#codeen)
    - [Euclid copyright](#euclid-licence)
    - [Testing](#testing)
    - [Documentation](#documentation)

## Code sharing/versioning

Code sharing is one of the main tasks of the building of a pipeline.
The current [Euclid repository][euclidsvn], located on ESA servers, relies on [SVN][svncommands] for sharing and versioning code projects as well as external software.

Official instructions on where to commit code can be found on the following [page][codeorga].
For OU-MER, there are two directories of interest on the main ESA repository:

- the [MER sandbox directory](#third-party-software) where the MER third-party libraries / software should be stored (e.g. MER pipeline , *TPHOT*)
- the [MER official projects directory](#official-mer-project-euclid-prototype-compliant-software) (e.g. *Background*, *PSFHomogenization*, etc.)


### Initial upload of code on the repository

Let's assume the code of interest is located on your computer at `path_to_projects/ProjectName`.

#### Third-party software

- If the software is released under a GPL-like license (= with strong copyleft) **and**  is planned to be integrated to the Euclid pipeline (= not a temporary code), please refer first to the [licencing](#third-party-software-under-gpl-like-license) rules (this is **very important**)

- Upload the full project to the http://euclid.esac.esa.int/svn/EC/SGS/OU/MER repository:

    ```
    SVN_OU_REPO=http://euclid.esac.esa.int/svn/EC/SGS/OU/MER

    cd path_to_projects
    svn import -m "Initial import of ProjectName" ./ProjectName $SVN_OU_REPO/ProjectName
    ```


#### Official MER project (Euclid prototype / compliant software)

- Go to the main project directory and make sure a SVN structure (tags/branches/trunk) is implemented
    ```
    cd path_to_projects

    tree -L 2 ProjectName
    ProjectName/
    ├── branches
    ├── tags
    └── trunk
        ├── CMakeLists.txt
        ├── Makefile
        └── PackageName
    ```

- Upload the full project to the http://euclid.esac.esa.int/svn/EC/SGS/PF/MER repository
    ```
    SVN_PF_REPO=http://euclid.esac.esa.int/svn/EC/SGS/PF/MER

    svn import -m "PF MER: importing new project ProjectName" ./ProjectName $SVN_PF_REPO/ProjectName
    ```


### Working on an existing project

Let's assume the code of interest is located on the ESA SVN repo at `http://euclid.esac.esa.int/svn/path_to_project/ProjectName`

- download the Project repository in the desired directory

    ```
    cd desired_dir
    svn checkout http://euclid.esac.esa.int/svn/path_to_project/ProjectName ProjectName
    ```

Then, after modifications to the code:

- update to merge changes on the online repo with your local version and deal with the possible conflicts before publishing your modifications

    ```
    cd desired_dir/ProjectName
    svn update
    ```

- commit your changes with an appropriate message describing the changes

    ```
    cd desired_dir/ProjectName
    svn commit -m "Bla bla"
    ```


## Euclid Development

General guidelines can be found on the [coding standards page](http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Cod_Std-v03)

**TBC**

### EDEN

EDEN (Euclid Development ENvironment) is the referential of common standards (OS, libs, etc.), rules (coding standards, etc.), tools (compilers, packaging, etc.) and corresponding versions that should be applied to develop Euclid software.

The [EDEN wiki page][edenpage] provides a list of the currently accepted compilers and software libraries with their corresponding version. These versions are already installed on LODEEN.

If there is a good reason why you think a specific library/software should be included in this list, please send a [change request][mailto:codeen-support@lists.euclid-ec.org] with a well described motivation and application examples

> Originator:
> Missing feature:
> Use case:
> Justification:
> Proposed solution:


### LODEEN

*Extracted from LODEEN introduction [page][lodeenintro]:*

> LODEEN: LOcal DEvelopment ENvironment.
> It is **a Virtual Machine image of a ready to use Desktop** for Euclid Software developement on top of developers laptop.

> The main goal of LODEEN is to **provide the same environment for all Euclid developers**. This environment has to be as close as possible to the production environment, so developers avoid compatibility problems of their code on production servers.

> LODEEN is a virtual machine with a ~~Scientific Linux~~ CentOS desktop, where we add some compilers, softwares and scientific libraries to be [EDEN](#eden)-compliant. We added an Integrated Development Environment (with support for C++ and python) and a platform to manage code quality. You can also manage your jobs in [CODEEN](#codeen) directly from LODEEN.

> You can use this Virtual Machine to:

>  - Install/upgrade [EDEN](#eden) libraries
>  - Build a project (support for C++ and python code)
>  - Run unit/smoke [tests](#testing)
>  - Perform quality check on your code
>  - Generate dashboards from quality check
>  - Synchronize your project with your [SVN repository](#svn) (and compare/update/commit/...)
>  - Start a remote build of the corresponding job on the CODEEN-jenkins platform and get results


Information on how to install LODEEN can be found [here][lodeeninstall].

### Elements

See build tool [wiki page](http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Bui_Too).

**TBC**

### Tutorial

An excellent introduction to the Euclid development environment has been prepared for the second edition of the [Developer Workshop][devworkshop2] (October 2015, Geneva) in the form of a tutorial.

It is highly recommanded to follow this entire [tutorial][quickstarttuto] before starting your first project on LODEEN.


## Euclid compliant code

### CODEEN

**TBD**

#### Jenkins

[Jenkins][jenkins]

**TBD**

#### CODEEN form

In order for your project to be build nightly you should fill in and send the following form to [this address](mailto:commontools.support@lists.euclid-ec.org).

> Name of the project :

> Description of the project :

> Origin of the project :

> Associated WP Id and name :

> Name of the development manager :

> Name of development team members :

> Source code language(s) :

> Source code repository :

> Specific dependencies :

> Buiding tool :

> Unit tests provided :

> doxygen tags provided :

> Target platform(s) :

> Wish a Redmine subproject ? :

#### Name checking and registering

Additionally, it is recommended to register every new project at [this address][namecheck].

### Euclid licence

As required [here][licencerule], a copy of the following licence **must** be inserted at the beginning of each source code file as a comment.

```
Copyright (C) 2012-2020 Euclid Science Ground Segment

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 3.0 of the License, or (at your option)
any later version.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
```

#### Third-party software under GPL-like license

The LGPL license chosen by the Euclid SGS is a license with weak copyleft, meaning that if developers wish to reuse some of the Euclid SGS legacy code in their own code, they are not required to release their source code, contrary to the GPL licence (strong copyleft) which requires further modifications to the code to be also released under the GPL license (and thus be open sourced). More information on this can be found [here][lgplvsgpl].

This also means that **if one wishes to integrate a code under the GPL license into the Euclid SGS stack, it is mandatory to obtain an authorization signed by the author(s) of the code**, so that it can exceptionnally be distributed under the LGPL license.


You will find hereby the pattern of letter that must be provided with the code and signed by the author(s) of the software.

  > Dear Sir,
  >
  > I, the undersigned, M. XXX (title, position in the institute or company), certify that I’m the author of (software name) which is distributed under GPL v.3 license.
  >
  > The software is (Software package description + coding langage, documentation, design)
  >
  > Within the framework of Euclid ground system development, I authorize Euclid Consortium, by special dispensation, to integrate the software cited above under LGPL v.3 license. This authorization is granted only for the Euclid ground system development.
  >
  > Best regards

**WARNING:** I have recently sent an email asking for precisions on this specific topic. This will be updated accordingly.

### Testing

See tests and verification [wiki page](http://euclid.roe.ac.uk/projects/codeen-users/wiki/SEG#11-Tests-Verification-38-Validation)

**TBC**

### Documentation

**TBD**

---

[![ccbysalogo][imgccbysa]][ccbysa] This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License][ccbysa]

[ccbysa]: http://creativecommons.org/licenses/by-sa/4.0/
[imgccbysa]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons License"


[mainpage]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/General_Resources "General resources"
[euclidsvn]: http://euclid.esac.esa.int/svn/EC/SGS "Euclid SVN repo"
[svncommands]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/SVN_commands "SVN commands"

[edenpage]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/EDEN "EDEN wiki page"
[scilib]: http://euclid.roe.ac.uk/projects/sgssteassau/wiki/ScientificLibraries "Common scientific libraries"
[lodeenpage]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Lodeen "LODEEN wiki page"
[lodeenintro]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Lodeen-intro "LODEEN introduction"
[lodeeninstall]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Lodeen-installUpgrade "LODEEN installation instructions"
[quickstarttuto]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/Developer_quick_start_tutorial
[devworkshop2]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/DevelopersWorkshop2 "DevelopersWorkshop2"

[getstarted]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/CMgettingStarted
[newproject]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/CM_Creating_new_project "New project howto"
[codeorga]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/SEG#122-Source-code-organization "Source code organization"
[codeenpage]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Pres "CODEEN wiki page"
[jenkins]: https://apceucliddev.in2p3.fr/jenkins/ "Jenkins page"
[namecheck]: https://pieclddj00.isdc.unige.ch/euclidnaming/NameCheck/ "Name checking"
[licencerule]: http://euclid.roe.ac.uk/projects/codeen-users/wiki/User_Cod_Std-cppandpythonstandards-v1-0#COMLAYOUT-10-M-All-files-MUST-include-copyright-and-licence-information "Copyright information"
[lgplvsgpl]: http://www.gnu.org/licenses/license-recommendations.html "License recommendations"
